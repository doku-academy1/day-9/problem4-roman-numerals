import java.util.*;

public class Main {
    public static void main(String[] args) {
        romanNumerals(1974);
        romanNumerals(2021);
        romanNumerals(1646);
        romanNumerals(5);

    }

    public static void romanNumerals(Integer number) {
        Map<Integer, String> mapNumber = new HashMap<>();
        mapNumber.put(1, "I");
        mapNumber.put(5, "V");
        mapNumber.put(10, "X");
        mapNumber.put(50, "L");
        mapNumber.put(100, "C");
        mapNumber.put(500, "D");
        mapNumber.put(1000, "M");

        List<Integer> numerals = Arrays.asList(1,5,10,50,100,500,1000);
        List<Integer> list = new ArrayList<>();
        String result = "";

        Integer repeat = 0;
        for (int i = numerals.size()-1; i >= 0; i--) {
            while (number - numerals.get(i) >= 0) {
                number -= numerals.get(i);
                list.add(numerals.get(i));
                repeat += 1;
            }
            if (repeat > 3) {
                result += mapNumber.get(numerals.get(i)) + mapNumber.get(numerals.get(i+1));
            } else {
                result += stringBuilder(mapNumber.get(numerals.get(i)), repeat);
            }
            repeat = 0;
        }

        System.out.println(result);
        System.out.println(list);

    }

    public static String stringBuilder(String s, Integer x) {
        String build = "";
        build += s.repeat(x);
        return build;
    }
}